Menu Cache Clear
=========

Menu Cache Clear is a module which clean cache by menu. User need to insert 
menu in given text field to clear the cache. This provides the most simplest 
way to clear the cache of particular menu.

INSTALLATION
------------

1. Download the module from http://drupal.org/project/menu_cache_clear and save it to
   your modules folder.
2. Ensure you have enabled and configured the Menu Cache Clear.
3. Enable the module at admin/modules.


USAGE
-----

1. Use For Cache Clear By Menu.
2. For clear cache by menu you can insert menu in given text field and the url 
of module is admin/config/content/menu_cache_clear
3. You can change the validation message by this url admin/config/content/setting
